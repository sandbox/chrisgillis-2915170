<?php

/**
 * @file
 * Module file.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_entity_type_alter().
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
 *   Entity Types.
 */
function machine_name_property_entity_type_alter(array &$entity_types): void {
  foreach (_machine_name_property_get_entity_types() as $entity_type) {
    if ($entity_types[$entity_type]) {
      _machine_name_property_add_entity_key($entity_type, $entity_types);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function machine_name_property_form_alter(array &$form, FormStateInterface $form_state): void {
  $forms_to_alter = array_map(function ($a) {
    return $a . '_form';
  }, _machine_name_property_get_entity_types());
  if (!isset($form_state->getBuildInfo()['base_form_id']) || !in_array($form_state->getBuildInfo()['base_form_id'], $forms_to_alter)) {
    return;
  }

  /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
  $entity = $form_state->getFormObject()->getEntity();
  $default_value = $entity->get('machine_name')->first() ? $entity->get('machine_name')->first()->getValue()['value'] : '';

  $form['machine_name'] = [
    '#type' => 'machine_name',
    '#default_value' => $default_value,
    '#maxlength' => EntityTypeInterface::ID_MAX_LENGTH,
    '#machine_name' => [
      'exists' => '_machine_name_property_exists_callback',
      'source' => ['label', 'widget', 0, 'value'],
    ],
    '#description' => t('A unique machine-readable name for this entity. It must only contain lowercase letters, numbers, and underscores.'),
  ];
}

/**
 * Implements hook_entity_base_field_info().
 */
function machine_name_property_entity_base_field_info(EntityTypeInterface $entity_type): array {
  $fields = [];
  if (in_array($entity_type->id(), _machine_name_property_get_entity_types(), TRUE)) {
    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine Name'))
      ->setDescription(t('Machine Name.'))
      ->setRequired(TRUE)
      ->setCardinality(1);
  }
  return $fields;
}

/**
 * Callback to check if machine name exists.
 *
 * @param string $machine_name
 *   The machine name to check.
 * @param array $element
 *   The form element.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 *
 * @return \Drupal\Core\Entity\ContentEntityInterface[]
 *   Array of matching entities.
 */
function _machine_name_property_exists_callback(string $machine_name, array $element, FormStateInterface $form_state): array {
  $entity_type = $form_state->getFormObject()->getEntity()->getEntityTypeId();
  $existing = \Drupal::entityTypeManager()
    ->getStorage($entity_type)
    ->loadByProperties(['machine_name' => $machine_name]);
  return $existing;
}

/**
 * Set the "machine_name" entity key.
 *
 * @param string $entity_type
 *   Entity Type.
 * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
 *   Entity Types.
 */
function _machine_name_property_add_entity_key(string $entity_type, array &$entity_types): void {
  $entity_keys = $entity_types[$entity_type]->get('entity_keys');
  $entity_keys = array_merge($entity_keys, ['machine_name' => 'machine_name']);
  $entity_types[$entity_type]->set('entity_keys', $entity_keys);
}

/**
 * Get an array of entity types from config.
 */
function _machine_name_property_get_entity_types(): array {
  $entity_types = \Drupal::config('machine_name_property.settings')->get('entity_types');
  return $entity_types ?? [];
}
