This module is a tool for providing a machine name as a property (Base Field) on
content entities.

There is no configuration form for this module... you have to edit the
configuration yourself to tell the module which entities you would like the
machine_name property applied to.

We are currently making one assumption about the entity: We assume it has a
"label" property (standard content entity title field).

## Similar Modules
[safeword](https://www.drupal.org/project/safeword) and
[machine_name](https://www.drupal.org/project/machine_name)
are both modules which use the field api. This module adds a base field instead
for more efficient sql queries.

## Requirements
* PHP 7.1

## Installation
* Install module as normal.
* Edit machine_name_property.settings.yml in your config directory.
* The entity_types key should be an array of types you want the property added to.
